#include <ncurses.h>
#include <stdlib.h> // pour exit
#include <signal.h>

#include "2048.h"
#include "libft.h"

static void handle_sigkill(int num) {
    (void)num;
    destroy_sys();
}

static void handle_sigint(int num) {
    (void)num;
    destroy_sys();
}

static void handle_resize(int num) {
    (void)num; // UNUSED
    endwin();
    refresh();
    clear();
    t_ui_sys *ui = (t_ui_sys *)sys->ui;
    wclear(ui->win);
    wresize(ui->win, LINES, COLS);
    wborder(ui->win, '|', '|', '-', '-', '+', '+', '+', '+');
    wrefresh(ui->win);
    t_game_sys *game = (t_game_sys *)sys->game;
    // redraw
    draw_plateau(ui, game->plateau, game->rows);
}

static void put_tile(t_ui_sys *sys, int plateau, unsigned inc_x, unsigned inc_y) {
    char *str = ft_itoa(plateau);
    mvwaddstr(sys->win, 1 + inc_y, 1 + inc_x, str);
    wrefresh(sys->win);
    free(str);
}

void draw_plateau (t_ui_sys *sys, int *plateau, unsigned size) {
    unsigned cur_x = (COLS / size);
    unsigned cur_y = (LINES / size);

    wmove(sys->win, 0, 0);
    wrefresh(sys->win);

    wclear(sys->win);
    wresize(sys->win, LINES, COLS);
    wborder(sys->win, '|', '|', '-', '-', '+', '+', '+', '+');
    wrefresh(sys->win);
    
    for (unsigned i = 0; i < size - 1; i++) {
        wmove(sys->win, 1, cur_x);
        wrefresh(sys->win);
        wvline(sys->win, '|', LINES - 2);
        wrefresh(sys->win);
        wmove(sys->win, cur_y, 1);
        wrefresh(sys->win);
        whline(sys->win, '-', COLS - 2);
        wrefresh(sys->win);

        cur_x += (COLS / size);
        cur_y += (LINES / size);
    }
    
    wmove(sys->win, 0, 0);
    wrefresh(sys->win);

    unsigned k = 0;
    for (unsigned i = 0; i < size; i++) {
        for (unsigned j = 0; j < size; j++) {
            if (plateau[k]) {
                put_tile(sys, plateau[k],
                         (COLS / size) * j, (LINES / size) * i);
            }
            k++;
        }
    }
}

void handle_keys (t_ui_sys *sys, int ch) {
    t_game_sys *game_sys = (t_game_sys *)sys->opaque_sys;
    switch (ch) {
        case KEY_UP:
            game_sys->direction = MOVE_UP;
            sys->key_up(game_sys);
            break;
        case KEY_DOWN:
            game_sys->direction = MOVE_DOWN;
            sys->key_down(game_sys);
            break;
        case KEY_LEFT:
            game_sys->direction = MOVE_LEFT;
            sys->key_left(game_sys);
            break;
        case KEY_RIGHT:
            game_sys->direction = MOVE_RIGHT;
            sys->key_right(game_sys);
            break;
        case 27:
            sys->key_esc(game_sys);
            break;
        default:
            if (sys->refresh_ui)
                sys->refresh_ui(game_sys);
            break;        
    }
    refresh();
}

int init_ncurses(t_ui_sys *sys) {
    initscr();
    clear();
    noecho();
    cbreak();
    curs_set(0);

    // LINES et COLS sont des globales définies par ncurses
    sys->win = newwin(LINES, COLS, 0, 0);
    if (!sys->win) {
        return 0;
    }
    keypad(sys->win, TRUE);
    refresh();
    
    signal(SIGWINCH, handle_resize);
    signal(SIGKILL, handle_sigkill);
    signal(SIGKILL, handle_sigint);
    wborder(sys->win, '|', '|', '-', '-', '+', '+', '+', '+');
    wrefresh(sys->win);

    return 1;
}
