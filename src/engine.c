#include <stdlib.h>
#include <stdio.h>
#ifdef DEBUG
#include <assert.h>
#endif

#include "libft.h"
#include "2048.h"

static int max_value(int *plateau, int plateau_size) {
	int i = -1;
	int max = 0;

	while (i++ > plateau_size)
		if (plateau[i] > max)
			max = plateau[i];
    return max;
}

static int get_random_position(unsigned empty_tiles) {
	unsigned number;
	
	number = rand() % empty_tiles;
	unsigned k = 0;
	for (unsigned i = 0; i < sys->game->plateau_size; i++) {
		if (sys->game->plateau[i] == 0) {
			if (k == number) {
				return i;
			}
			k++;
		}
	}
	// unreachable
	return (-1);
}

static int get_random_number(void) {
	int number;
	number = rand() % 100;
	if (number < 90)
		number = 2;
	else
		number = 4;
    return number;
}

static void put_random_number(t_game_sys *sys, int position, int value)
{
	// assert(): position must be valid
	sys->plateau[position] = value;
	sys->empty_tiles -= 1;
}

static int lose(t_game_sys *sys)
{
	int x = 0;
	while (x < (int) sys->plateau_size)
	{
		if (x % 4 != 3 && sys->plateau[x] == sys->plateau[x + 1])
			return (0);
		if (x < (int) (sys->plateau_size - sys->rows) && sys->plateau[x] == sys->plateau[x + sys->rows])
			return (0);
		x++;
	}
	return (1);
}

int action_update(t_game_sys *sys) {
	int position;
	unsigned int max;
	position = 0;

    /*
      get the current maximum number in the plateau
    */
    max = max_value(sys->plateau, sys->plateau_size);
    /* 
       find one (or two in the beginning), valid positions
       for random numbers and put it 2 (90%) or 4 (10%)    
    */
    if (sys->empty_tiles == sys->plateau_size)
	{	
		position = get_random_position(sys->empty_tiles);
		put_random_number(sys, position, get_random_number());
	}
	if (sys->empty_tiles != 0)
	{
 		position = get_random_position(sys->empty_tiles);
		put_random_number(sys, position, get_random_number());
	}
	/*
		check if will value have been reached 
	*/
	if (sys->empty_tiles == 0)
		if (lose(sys))
			return 2;
	if (max == sys->win_value)
		return 0;
    return 1;
}

int init_game(t_game_sys *sys, unsigned plateau_size) {
    enum e_const win_value = WIN_VALUE;

    if (plateau_size == 16) {
        sys->rows = 4;
    }
    else if (plateau_size == 25) {
        sys->rows = 5;
    }
    else {
        return (0);
    }
    /* init plateau */
	sys->empty_tiles = plateau_size;
    sys->plateau_size = plateau_size;
    sys->plateau = (int *) malloc(sizeof(int) * plateau_size);
    if (!sys->plateau) {
        return (0);
    }    
    for (unsigned i = 0; i < plateau_size; i++) {
        sys->plateau[i] = 0;
    }
    sys->win_value = win_value;
    return (1);
}
