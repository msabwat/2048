/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ascii.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ldubuche <laura.dubuche@gmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/20 15:26:28 by ldubuche          #+#    #+#             */
/*   Updated: 2022/03/20 16:19:36 by ldubuche         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "2048.h"

static void	print_val_2(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "     _      ");
	mvaddstr(row + 2, col, "     _|     ");
	mvaddstr(row + 3, col, "    |_      ");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_4(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "            ");
	mvaddstr(row + 2, col, "    |_|     ");
	mvaddstr(row + 3, col, "      |     ");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_8(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "     _      ");
	mvaddstr(row + 2, col, "    |_|     ");
	mvaddstr(row + 3, col, "    |_|     ");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_16(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "       _    ");
	mvaddstr(row + 2, col, "     ||_    ");
	mvaddstr(row + 3, col, "     ||_|   ");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_32(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "    _  _    ");
	mvaddstr(row + 2, col, "    _| _|   ");
	mvaddstr(row + 3, col, "    _||_    ");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_64(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "    _       ");
	mvaddstr(row + 2, col, "   |_ |_|   ");
	mvaddstr(row + 3, col, "   |_|  |   ");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_128(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "    _  _    ");
	mvaddstr(row + 2, col, " |  _||_|  	");
	mvaddstr(row + 3, col, " | |_ |_|  	");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_256(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "  _  _  _   ");
	mvaddstr(row + 2, col, "  _||_ |_   ");
	mvaddstr(row + 3, col, " |_  _||_|  ");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_512(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "  _     _   ");
	mvaddstr(row + 2, col, " |_   | _|  ");
	mvaddstr(row + 3, col, "  _|  ||_   ");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_1024(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "    _  _   ");
	mvaddstr(row + 2, col, "  || | _||_|");
	mvaddstr(row + 3, col, "  ||_||_   |");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_2048(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, " _  _     _");
	mvaddstr(row + 2, col, " _|| ||_||_|");
	mvaddstr(row + 3, col, "|_ |_|  ||_|");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_4096(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, "    _  _  _");
	mvaddstr(row + 2, col, "|_|| ||_||_ ");
	mvaddstr(row + 3, col, "  ||_| _||_|");
	mvaddstr(row + 4, col, "            ");
}

static void	print_val_8192(const int row, const int col)
{
	mvaddstr(row, col, "            ");
	mvaddstr(row + 1, col, " _     _  _");
	mvaddstr(row + 2, col, "|_|  ||_| _|");
	mvaddstr(row + 3, col, "|_|  | _||_ ");
	mvaddstr(row + 4, col, "            ");
}

void	afficher_ascii(int row, int col, int value)
{
	switch (value) {
		case 2:
			print_val_2(row, col);
		case 4:
			print_val_4(row, col);
		case 8:
			print_val_8(row, col);
		case 16:
			print_val_16(row, col);
		case 32:
			print_val_32(row, col);
		case 64:
			print_val_64(row, col);
		case 128:
			print_val_128(row, col);
		case 256:
			print_val_256(row, col);
		case 512:
			print_val_512(row, col);
		case 1024:
			print_val_1024(row, col);
		case 2048:
			print_val_2048(row, col);
		case 4096:
			print_val_4096(row, col);
		case 8192:
			print_val_8192(row, col);
		default:
			mvaddstr(row, col, ft_itoa(value));
	}
}
