/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/20 10:48:55 by ldubuche          #+#    #+#             */
/*   Updated: 2022/03/20 22:55:14 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../2048.h"
#include <stdio.h>

void move_left(t_game_sys *sys)
{
	int x = 0;
	int y = 0;
	int j = 0;

	while (x < (int) sys->plateau_size)
	{
		y = x;
		while (y < (int) (x + sys->rows))
		{
			if (sys->plateau[y] != 0)
			{
				j = y + 1;
				while (j < (int) (x + sys->rows - 1) && sys->plateau[j] == 0)
					j ++;
				if (y < (int) (x + sys->rows - 1) && sys->plateau[j] == sys->plateau[y])
				{
					sys->plateau[y] = sys->plateau[j] * 2;
					sys->plateau[j] = 0;
					sys->empty_tiles++;
				}
			}
			y ++;
		}
		x += sys->rows;
	}
	x = 0;
	while (x < (int) sys->plateau_size)
	{
		y = x;
		while (y < (int) (x + sys->rows))
		{
			if (sys->plateau[y] != 0)
			{
				if (y != x && sys->plateau[y - 1] == 0)
				{
					sys->plateau[y - 1] = sys->plateau[y];
					sys->plateau[y] = 0;
					
					y--;
				}
				else
					y++;
			}
			else
				y++;
		}
		x += sys->rows;
	}
}

void move_right(t_game_sys *sys)
{
	
	int x = 0;
	int y = 0;
	int j = 0;

	while (x < (int) sys->plateau_size)
	{
		y = x + sys->rows - 1;
		while (y >= x)
		{
			if (sys->plateau[y] != 0)
			{
				j = y - 1;
				while (j > x && sys->plateau[j] == 0)
				{
					j --;
				}
				if (y > x && sys->plateau[j] == sys->plateau[y])
				{
					sys->plateau[y] = sys->plateau[j] * 2;
					sys->plateau[j] = 0;
					sys->empty_tiles++;
				}
			}
			y --;
		}
		x += sys->rows;
	}
	x = 0;
	while (x < (int) sys->plateau_size)
	{
		y = x + sys->rows - 1;
		while (y >= x)
		{
			if (sys->plateau[y] != 0)
			{
				if (y != (int) (x + sys->rows - 1) && sys->plateau[y + 1] == 0)
				{
					sys->plateau[y + 1] = sys->plateau[y];
					sys->plateau[y] = 0;
					y++;
				}
				else
					y--;
			}
			else
				y--;
		}
		x += sys->rows;
	}
}

void move_up(t_game_sys *sys)
{
	int x = 0;
	int y = 0;
	int j = 0;

	while (x < (int) sys->rows)
	{
		y = x;
		while (y < (int) sys->plateau_size)
		{
			if (sys->plateau[y] != 0)
			{
				j = y + sys->rows;
				while (j < (int) (x + sys->rows * (sys->rows - 1)) && sys->plateau[j] == 0)
				{j += sys->rows;}
					
				if (y != (int) (x + sys->rows * (sys->rows - 1)) && sys->plateau[j] == sys->plateau[y])
				{
					sys->plateau[y] = sys->plateau[j] * 2;
					sys->plateau[j] = 0;
					sys->empty_tiles++;
				}
			}
			y += sys->rows;
		}
		x++;
	}
	x = 0;
	while (x < (int) sys->rows)
	{
		y = x;
		while (y < (int) sys->plateau_size)
		{
			if (sys->plateau[y] != 0)
			{
				if (y != x && sys->plateau[y - sys->rows] == 0)
				{
					sys->plateau[y - sys->rows] = sys->plateau[y];
					sys->plateau[y] = 0;
					y -= sys->rows;
				}
				else
					y += sys->rows;
			}
			else
				y += sys->rows;
		}
		x++;
	}
}

void move_down(t_game_sys *sys)
{
	int x = 0;
	int y = 0;
	int j = 0;

	while (x < (int) sys->rows)
	{
		y = x + sys->rows * (sys->rows - 1);
		while (y >= x)
		{
			if (sys->plateau[y] != 0 && y != x)
			{
				j = y - sys->rows;
				while (j > x && sys->plateau[j] == 0)
				{
					j -= sys->rows;
				}
				if (y >= x && sys->plateau[j] == sys->plateau[y])
				{
					sys->plateau[y] = sys->plateau[j] * 2;
					sys->plateau[j] = 0;
					sys->empty_tiles++;
				}
			}
			y -= sys->rows;
		}
		x++;
	}
	x = 0;
	while (x < (int) sys->rows)
	{
		y = x + sys->rows * (sys->rows - 1);
		while (y >= x)
		{
			if (sys->plateau[y] != 0)
			{
				if (y != (int) (x + sys->rows * (sys->rows - 1)) && sys->plateau[y + sys->rows] == 0)
				{
					sys->plateau[y + sys->rows] = sys->plateau[y];
					sys->plateau[y] = 0;
					y += sys->rows;
				}
				else
					y -= sys->rows;
			}
			else
				y -= sys->rows;
		}
		x++;
	}
}

void action_move(void *sys) 
{
    t_game_sys *game = (t_game_sys *)sys;
    t_direction direction = game->direction;
    
	if (direction == MOVE_UP) 
	{
		move_up(game);
    }
    else if (direction == MOVE_DOWN)
	{
    	move_down(game); 
    }
    else if (direction == MOVE_LEFT)
	{
        move_left(game);
    }
    else if (direction == MOVE_RIGHT)
	{
        move_right(game);
		
    }
}
