#include <ncurses.h>
#include <stdlib.h>
#include <time.h>

#include "2048.h"

t_sys *sys = NULL;

// hacks
static void wrap_draw_plateau(void *opq) {
  (void)opq;
  draw_plateau(sys->ui, sys->game->plateau, sys->game->rows);
}

static void wrap_escape(void *opq) {
  (void)opq;
  destroy_sys();
}


static t_sys *init_sys(void) {
  sys = (t_sys *)malloc(sizeof(t_sys));
  if (!sys) {
    return NULL;
  }
  sys->game = (t_game_sys *)malloc(sizeof(t_game_sys));
  if (!sys->game) {
    free(sys);
    return NULL;
  }
  sys->game->plateau = NULL;
  sys->game->game_state = false;
  sys->ui = (t_ui_sys *)malloc(sizeof(t_ui_sys));
  if (!sys->ui) {
    free(sys);
    free(sys->game);
    return NULL;
  }
  sys->ui->opaque_sys = sys->game;
  sys->ui->refresh_ui = wrap_draw_plateau;
  sys->ui->key_up = action_move;
  sys->ui->key_down = action_move;
  sys->ui->key_left = action_move;
  sys->ui->key_right = action_move;
  sys->ui->key_esc = wrap_escape;
  return sys;
}

static void destroy_ui(void) {
    endwin();
    delwin(sys->ui->win);
    free(sys->ui);
}

static void destroy_game(void) {
    free(sys->game->plateau);
    free(sys->game);
}

void destroy_sys(void) {
    if (sys) {
        if (sys->ui) {
            destroy_ui();
        }
        if (sys->game) {
            destroy_game();
        }
    }
    free(sys);
}

int main()
{
  /* Initialisation rand() */
  srand(time(NULL));
  sys = init_sys();
  if (!sys) {
    return 1;
  }

  init_ncurses(sys->ui);
  init_game(sys->game, SIZE);
  action_update(sys->game);
  wrefresh(sys->ui->win);
  int c = 0;
  int ret = 0;
  sys->game->game_state = true;
  while (sys->game->game_state) {
    draw_plateau(sys->ui, sys->game->plateau, sys->game->rows);
    c = wgetch(sys->ui->win);
    handle_keys(sys->ui, c);
    ret = action_update(sys->game);
    if (ret == 2) {
      endwin();
      wclear(sys->ui->win);
      destroy_sys();
      printf("\nGAME OVER !\n");
      return 1;
    }
    else if (ret == 0) {
      wclear(sys->ui->win);
      mvwprintw(sys->ui->win, 1, 1, "Congratulations, you reached %d ! If you want to give up, press ESC", sys->game->win_value);
      wrefresh(sys->ui->win);
      int c2 = getch(); (void)c2;
      continue;
    }
	}
	clrtoeol();
	refresh();
  wrefresh(sys->ui->win);
  destroy_sys();
  return 0;
}