NAME=2048

FLAGS=-Wall -Wextra -Werror

CC=cc

INC=-I.

LIBFT_PATH=libft/

LIBFT_INC=-I$(LIBFT_PATH)

LIBFT_LINK_FLAGS=-L $(LIBFT_PATH) -lft 

SRC_NAME= engine.c \
		  ui.c \
		  move.c

OBJ_NAME=$(SRC_NAME:.c=.o)

SRC_PATH=src

OBJ_PATH=.obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME)) 

DEPS=$(LIBFT_LINK_FLAGS) -lncurses

TEST=

HEADER = 2048.h

all: makedir $(NAME)

makedir:
	@mkdir -p .obj

test: FLAGS += -g -fsanitize=address,undefined -DDEBUG
test: TEST = test
test: makedir $(NAME)


$(NAME): $(OBJ_PATH)/main.o $(OBJS)
	$(MAKE) -C $(LIBFT_PATH)
	$(CC) $(FLAGS) $(OBJ_PATH)/main.o $(LIBFT_INC) $(OBJS) $(INC) $(DEPS) -o $(NAME)

$(OBJ_PATH)/main.o: main.c $(HEADER)
	 $(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c main.c -o $(OBJ_PATH)/main.o 
$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c $(HEADER)
	$(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c $< -o $@

clean:
	rm -fr $(NAME)

fclean: clean
	rm -fr $(OBJ_PATH)
	rm -fr main.o
	$(MAKE) fclean -C $(LIBFT_PATH)

re: fclean all
