#include <stdbool.h>
#include <limits.h>
#include <ncurses.h>

#define VALUE 8
#define SIZE 16
enum e_const {
    WIN_VALUE = VALUE
};

#if ((VALUE & (VALUE - 1)) != 0)
#error win_value not a power of 2
#elif (VALUE <= 0)
#error win_value not a positive number
#elif (VALUE > INT_MAX)
#error win_value too big
#endif

typedef enum e_direction {
    MOVE_UP,
    MOVE_DOWN,
    MOVE_LEFT,
    MOVE_RIGHT
} t_direction;

typedef struct s_game_sys {
    int *plateau;
    unsigned rows;
    unsigned plateau_size;
    unsigned empty_tiles;
    /* pour interrompre le programme si besoin */
    bool game_state;
    t_direction direction;
    enum e_const win_value;
} t_game_sys;

typedef struct s_ui_sys {
    void *opaque_sys;
    
    void (*refresh_ui)(void *);
    void (*key_up)(void *);
    void (*key_down)(void *);
    void (*key_left)(void *);
    void (*key_right)(void *);
    void (*key_esc)(void *);

    WINDOW *win;
} t_ui_sys;

typedef struct s_sys {
  t_game_sys *game;
  t_ui_sys *ui;
} t_sys;

extern t_sys *sys;


/* moteur de jeu */
int init_game(t_game_sys *sys, unsigned plateau_size);
void action_move(void *sys);
/* 
   c'est la valeur de retour de cette fonction qui va déterminer le
   WIN/LOSE. Mais ça ne veut pas forcément dire qu'on mettra sys->game_state
   à false
*/
int action_update(t_game_sys *sys);

/* interface */
void draw_plateau (t_ui_sys *sys, int *plateau, unsigned size);
void handle_keys (t_ui_sys *sys, int ch);
int init_ncurses(t_ui_sys *sys);

void destroy_sys(void);
