/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ldubuche <laura.dubuche@gmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/20 16:20:17 by ldubuche          #+#    #+#             */
/*   Updated: 2022/03/20 16:33:01 by ldubuche         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLOR_H
# define COLOR_H

# define COLOR_2 206
# define COLOR_4 208
# define COLOR_8 209
# define COLOR_16 210
# define COLOR_32 211
# define COLOR_64 212
# define COLOR_128 213
# define COLOR_256 214
# define COLOR_512 215
# define COLOR_1024 216
# define COLOR_2048 217
# define COLOR_4096 218
# define COLOR_8192 219

#endif